# Simulating Residential Heat Consumption With Spatially Referenced Synthetic Microdata

[Marcelo Esteban Hidalgo Muñoz, M.Sc.](marcelo.hidalgo@hcu-hamburg.de)

Technical Urban Infrastructure Systems Group,

HafenCity Universität, Hamburg, DE

Advisor: Prof. Irene Peters, Ph.D.

Second Advisor: Prof. Robert Tanton, Ph.D.